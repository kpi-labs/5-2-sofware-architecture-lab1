import os

from PIL import Image


def concat_images(*images):
    widths, heights = zip(*(i.size for i in images))

    total_width = sum(widths)
    max_height = max(heights)

    new_im = Image.new('RGB', (total_width, max_height))

    x_offset = 0
    for im in images:
        new_im.paste(im, (x_offset, 0))
        x_offset += im.size[0]
    return new_im


def show_both(*imgs):
    concat_images(*imgs).show()


def resolve_path(path):
    return os.path.join(os.path.dirname(__file__), '..', path)
