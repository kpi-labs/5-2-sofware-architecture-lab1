from enum import Enum

class ColorEnum(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3
