import numpy as np
from PIL import Image

from lab1.filters.AbstractFilter import AbstractFilter


class AddToneFilter(AbstractFilter):
    def apply(self, value, color):
        pixels = np.array(self.original)

        color_idx = color.value - 1

        def modify_pixel(pixel):
            modified = pixel
            modified[color_idx] = min(pixel[color_idx] + value, 255)
            return modified

        for i in range(len(pixels[0])):
            for j in range(len(pixels[1])):
                pixels[i, j] = modify_pixel(pixels[i, j])

        return Image.fromarray(pixels)
