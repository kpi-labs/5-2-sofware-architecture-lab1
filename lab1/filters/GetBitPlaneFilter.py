import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

from lab1.filters.AbstractFilter import AbstractFilter


class GetBitPlaneFilter(AbstractFilter):
    def apply(self, bit):
        data = np.array(self.original)
        extracted = (data[..., 0] ^ data[..., 1] ^ data[..., 2]) & (1 << (bit - 1))
        fig = plt.figure()

        fig.figimage(extracted)
        fig.canvas.draw()
        w, h = fig.canvas.get_width_height()
        buf = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8)
        buf.shape = (w, h, 3)
        buf = np.roll(buf, 3, axis=2)
        return Image.frombytes("RGB", (w, h), buf.tostring())
