import numpy as np
from PIL import Image

from lab1.filters.AbstractFilter import AbstractFilter


class KernelFilter(AbstractFilter):
    def apply(self, kernel):
        # Load image:
        input_pixels = np.array(self.original)
        width, height = self.original.size
        modified = input_pixels.copy()
        kernel = np.array(kernel)
        normalization_koef = np.sum(kernel)
        kernel = kernel / normalization_koef
        # Middle of the kernel
        offset = kernel.shape[0] // 2

        for x in range(offset, width - offset):
            for y in range(offset, height - offset):
                acc = [0, 0, 0]
                for a in range(len(kernel)):
                    for b in range(len(kernel)):
                        xn = x + a - offset
                        yn = y + b - offset
                        pixel = input_pixels[xn, yn]
                        acc[0] += pixel[0] * kernel[a, b]
                        acc[1] += pixel[1] * kernel[a, b]
                        acc[2] += pixel[2] * kernel[a, b]

                modified[x, y] = (acc[0], (acc[1]), (acc[2]))

        return Image.fromarray(modified)
