from abc import ABC, abstractmethod


class AbstractFilter(ABC):
    def __init__(self, original):
        self.original = original

    @abstractmethod
    def apply(self, *args):
        pass
