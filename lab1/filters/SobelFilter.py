import math

import numpy as np
from PIL import Image

from lab1.filters.AbstractFilter import AbstractFilter


class SobolFilter(AbstractFilter):
    def apply(self):
        # Load image:
        input_pixels = np.array(self.original)
        width, height = self.original.size
        # Calculate pixel intensity as the average of red, green and blue colors.
        intensity = [[sum(input_pixels[x, y]) / 3 for y in range(height)] for x in
                     range(width)]
        # Sobel kernels
        kernelx = [[-1, 0, 1],
                   [-2, 0, 2],
                   [-1, 0, 1]]

        kernely = [[-1, -2, -1],
                   [0, 0, 0],
                   [1, 2, 1]]

        # Compute convolution between intensity and kernels
        modified = np.zeros(input_pixels.shape)
        for x in range(1, width - 1):
            for y in range(1, height - 1):
                magx, magy = 0, 0
                for a in range(3):
                    for b in range(3):
                        xn = x + a - 1
                        yn = y + b - 1
                        magx += intensity[xn][yn] * kernelx[a][b]
                        magy += intensity[xn][yn] * kernely[a][b]

                # Draw in black and white the magnitude
                color = np.uint8(math.sqrt(magx ** 2 + magy ** 2))
                modified[x, y] = (color, color, color)
        return Image.fromarray(modified.astype(np.uint8))
