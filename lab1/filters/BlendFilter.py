import numpy as np
from PIL import Image

from lab1.filters.AbstractFilter import AbstractFilter


class BlendFilter(AbstractFilter):
    def apply(self, opaque, add_img):
        pixels = np.array(self.original)
        add_pixels = np.array(add_img)

        # scale 2nd image to fit first

        add_img_opaque = max(1 - opaque, 0)

        def modify_pixel(pixel, add_pixel):
            return tuple([pixel[i] * opaque + add_pixel[i] * add_img_opaque for i in range(pixel.size)])

        for i in range(len(pixels[0])):
            for j in range(len(pixels[1])):
                pixels[i, j] = modify_pixel(pixels[i, j], add_pixels[i, j])

        return Image.fromarray(pixels)
