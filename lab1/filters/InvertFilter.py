import numpy as np
from PIL import Image

from lab1.filters.AbstractFilter import AbstractFilter


class InvertFilter(AbstractFilter):
    def apply(self):
        pixels = np.array(self.original)

        for i in range(self.original.size[0]):
            for j in range(self.original.size[1]):
                r, g, b = pixels[i, j][0], pixels[i, j][1], pixels[i, j][2]
                r, g, b = abs(r - 255), abs(g - 255), abs(b - 255)
                pixels[i, j] = (r, g, b)

        return Image.fromarray(pixels)
