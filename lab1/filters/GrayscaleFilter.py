import numpy as np
from PIL import Image

from lab1.filters.AbstractFilter import AbstractFilter


class GrayscaleFilter(AbstractFilter):
    @staticmethod
    def rgb2gray(rgb):
        return np.dot(rgb[..., :3], [0.2989, 0.5870, 0.1140])

    def apply(self):
        return Image.fromarray(self.rgb2gray(np.array(self.original)))
