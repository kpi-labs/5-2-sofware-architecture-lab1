from enum import IntEnum

import numpy as np
from PIL import Image, ImageDraw, ImageFont

from lab1.filters.AbstractFilter import AbstractFilter


class CoordEnum(IntEnum):
    X = 0
    Y = 1


class AddWatermarkFilter(AbstractFilter):
    def _create_black_and_white_image(self, watermark):
        input_pixels = watermark.load()
        modified = np.empty((*watermark.size, 3))
        for x in range(watermark.size[0]):
            for y in range(watermark.size[1]):
                pixel = input_pixels[x, y]
                r = int(pixel[0])
                g = int(pixel[1])
                b = int(pixel[2])
                s = (r + g + b)
                if s > (((255 + 100) // 2) * 3):
                    r, g, b = 255, 255, 255
                else:
                    r, g, b = 0, 0, 0
                modified[x, y] = (r, g, b)
        return modified

    def _apply_watermark_to_pixel(self, x, y, source, watermark, bit_number):
        source_pixel = source[x, y]
        source_blue = source_pixel[2]
        watermark_row = self._normalize_coord(x, CoordEnum.X, watermark.shape)
        watermark_col = self._normalize_coord(y, CoordEnum.Y, watermark.shape)
        watermark_blue = watermark[watermark_row, watermark_col][2]
        if watermark_blue == 255:
            return source_blue ^ (1 << (bit_number - 1))
        else:
            return source_blue

    def _normalize_coord(self, value, coord, watermark_size):
        return value % watermark_size[coord] if self.original.size[coord] > watermark_size[coord] else value

    def apply(self, watermark, bit_number):
        source_pixels = np.array(self.original)
        watermark_pixels = self._create_black_and_white_image(watermark)
        for x in range(self.original.size[0] - 1):
            for y in range(self.original.size[1] - 1):
                source_pixel = source_pixels[x, y]
                source_pixels[x, y] = (
                    source_pixel[0],
                    source_pixel[1],
                    self._apply_watermark_to_pixel(
                        x, y, source_pixels, watermark_pixels, bit_number)
                )

        img = Image.fromarray(source_pixels.astype(np.uint8))
        draw = ImageDraw.Draw(img)
        font = ImageFont.load_default()
        draw.text((0, 0), f"used bit {bit_number}", (255, 255, 255), font=font)
        return img
