import numpy as np
from PIL import Image

from lab1.filters.AbstractFilter import AbstractFilter


class CanalExtractorFilter(AbstractFilter):
    def apply(self, canal):
        pixels = np.array(self.original)

        canal_idx = canal.value - 1

        def modify_pixel(pixel):
            modified = [0, 0, 0]
            modified[canal_idx] = pixel[canal_idx]
            return tuple(modified)

        for i in range(len(pixels[0])):
            for j in range(len(pixels[1])):
                pixels[i, j] = modify_pixel(pixels[i, j])

        return Image.fromarray(pixels)
