import numpy as np
from PIL import Image

from lab1.filters.AbstractFilter import AbstractFilter


class MedianFilter(AbstractFilter):
    def apply(self, median_size):
        # Load image:
        input_pixels = np.array(self.original)
        width, height = self.original.size
        offset = median_size // 2
        modified = np.zeros(input_pixels.shape)
        for x in range(offset, width - offset):
            for y in range(offset, height - offset):
                acc = [[], [], []]
                for a in range(median_size):
                    for b in range(median_size):
                        xn = x + a - offset
                        yn = y + b - offset
                        pixel = input_pixels[xn, yn]
                        acc[0].append(pixel[0])
                        acc[1].append(pixel[1])
                        acc[2].append(pixel[2])
                acc[0].sort()
                acc[1].sort()
                acc[2].sort()
                modified[x, y] = (
                    int(acc[0][len(acc[0]) // 2]), int(acc[1][len(acc[1]) // 2]), int(acc[2][len(acc[2]) // 2]))

        return Image.fromarray(modified.astype(np.uint8))
