from math import sqrt

import numpy as np
from PIL import Image, ImageDraw

class Filter():

    def kernel_filter(self, source_path, result_path, kernel, normalization_koef = 1):
        kernel = np.array(kernel) / normalization_koef

        input_image = Image.open(source_path)
        input_pixels = input_image.load()

        # Middle of the kernel
        offset = len(kernel) // 2

        draw = ImageDraw.Draw(input_image)

        for x in range(offset, input_image.width - offset):
            for y in range(offset, input_image.height - offset):
                acc = [0, 0, 0]
                for a in range(len(kernel)):
                    for b in range(len(kernel)):
                        xn = x + a - offset
                        yn = y + b - offset
                        pixel = input_pixels[xn, yn]
                        acc[0] += pixel[0] * kernel[a][b]
                        acc[1] += pixel[1] * kernel[a][b]
                        acc[2] += pixel[2] * kernel[a][b]

                draw.point((x, y), (int(acc[0]), int(acc[1]), int(acc[2])))
        input_image.save(result_path)

    def median_filter(self, source_path, result_path, median_size):
        input_image = Image.open(source_path)
        input_pixels = input_image.load()

        offset = median_size // 2

        draw = ImageDraw.Draw(input_image)

        for x in range(offset, input_image.width - offset):
            for y in range(offset, input_image.height - offset):
                acc = [[], [], []]
                for a in range(median_size):
                    for b in range(median_size):
                        xn = x + a - offset
                        yn = y + b - offset
                        pixel = input_pixels[xn, yn]
                        acc[0].append(pixel[0])
                        acc[1].append(pixel[1])
                        acc[2].append(pixel[2])
                acc[0].sort()
                acc[1].sort()
                acc[2].sort()
                draw.point((x, y), (int(acc[0][len(acc[0]) // 2]), int(acc[1][len(acc[1]) // 2]), int(acc[2][len(acc[2]) // 2])))
        input_image.save(result_path)

    def sobol_filter(self, source_path, result_path):
        # Load image:
        input_image = Image.open(source_path)
        input_pixels = input_image.load()

        # Calculate pixel intensity as the average of red, green and blue colors.
        intensity = [[sum(input_pixels[x, y]) / 3 for y in range(input_image.height)] for x in range(input_image.width)]

        # Sobel kernels
        kernelx = [[-1, 0, 1],
                   [-2, 0, 2],
                   [-1, 0, 1]]
        kernely = [[-1, -2, -1],
                   [0, 0, 0],
                   [1, 2, 1]]

        draw = ImageDraw.Draw(input_image)

        # Compute convolution between intensity and kernels
        for x in range(1, input_image.width - 1):
            for y in range(1, input_image.height - 1):
                magx, magy = 0, 0
                for a in range(3):
                    for b in range(3):
                        xn = x + a - 1
                        yn = y + b - 1
                        magx += intensity[xn][yn] * kernelx[a][b]
                        magy += intensity[xn][yn] * kernely[a][b]

                # Draw in black and white the magnitude
                color = int(sqrt(magx ** 2 + magy ** 2))
                draw.point((x, y), (color, color, color))

        input_image.save(result_path)
