from PIL import ImageEnhance, Image, ImageDraw


class Watermark:

    def create_black_and_white_image(self, source_path):
        input_image = Image.open(source_path)
        input_pixels = input_image.load()
        draw = ImageDraw.Draw(input_image)
        for x in range(input_image.size[0]):
            for y in range(input_image.size[1]):
                pixel = input_pixels[x, y]
                r = pixel[0]
                g = pixel[1]
                b = pixel[2]
                s = r + g + b
                if (s > (((255 + 100) // 2) * 3)):
                    r, g, b = 255, 255, 255
                else:
                    r, g, b = 0, 0, 0
                draw.point((x, y), (r, g, b))
        return input_image

    def add_watermatk(self, source_path, watermark_path, bit_number, save_path):
        image = Image.open(source_path)
        watermark = self.create_black_and_white_image(watermark_path)
        source_pixels = image.load()
        target_pixels = watermark.load()
        print(watermark.height, watermark.width)
        for x in range(image.size[0] - 1):
            for y in range(image.size[1] - 1):
                source_pixel = source_pixels[x, y]
                image_blue = source_pixel[2]
                watermark_row = x % watermark.size[0] if image.size[0] > watermark.size[0] else x
                watermark_col = y % watermark.size[1] if image.size[1] > watermark.size[1] else y
                watermark_blue = target_pixels[watermark_row, watermark_col][2]
                if watermark_blue == 255:
                    resultB = image_blue ^ (1 << bit_number - 1)
                else:
                    resultB = image_blue
                source_pixels[x, y] = (source_pixel[0], resultB, source_pixel[2])
        image.save(save_path)
