from enum import IntEnum


class ColorEnum(IntEnum):
    RED = 1
    GREEN = 2
    BLUE = 3
