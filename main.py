import sys

from PIL import Image

from lab1.color_enum import ColorEnum
from lab1.filters.AddToneFilter import AddToneFilter
from lab1.filters.AddWatermarkFilter import AddWatermarkFilter
from lab1.filters.BlendFilter import BlendFilter
from lab1.filters.CanalExtractorFilter import CanalExtractorFilter
from lab1.filters.GetBitPlaneFilter import GetBitPlaneFilter
from lab1.filters.GrayscaleFilter import GrayscaleFilter
from lab1.filters.InvertFilter import InvertFilter
from lab1.filters.KernelFilter import KernelFilter
from lab1.filters.MedianFilter import MedianFilter
from lab1.filters.SobelFilter import SobolFilter
from lab1.utils import show_both, resolve_path

IMAGE_PATH = resolve_path('res/lenna.png')


def load_image(img_path):
    try:
        return Image.open(img_path)
    except Exception as e:
        print(f'Unable to load image {img_path}: ', e)
        return None


def invert(original_img):
    inverted_image = InvertFilter(original_img).apply()
    show_both(original_img, inverted_image)


def add_tone(original_img):
    red = AddToneFilter(original_img).apply(50, ColorEnum.RED)
    green = AddToneFilter(original_img).apply(50, ColorEnum.GREEN)
    blue = AddToneFilter(original_img).apply(50, ColorEnum.BLUE)
    show_both(original_img, red, green, blue)


def extract_canal(original_img):
    red = CanalExtractorFilter(original_img).apply(ColorEnum.RED)
    green = CanalExtractorFilter(original_img).apply(ColorEnum.GREEN)
    blue = CanalExtractorFilter(original_img).apply(ColorEnum.BLUE)
    show_both(original_img, red, green, blue)


def blend(original_img):
    add_img = load_image(resolve_path('res/example1.jpg'))
    if not add_img:
        return
    add_img = add_img.resize(original_img.size, Image.ANTIALIAS)

    show_both(original_img, add_img, BlendFilter(original_img).apply(0.5, add_img))


def watermark(original_img):
    wtm = load_image(resolve_path('res/watermark.jpg'))
    if not wtm:
        return
    wtm = wtm.resize((100, 200), Image.ANTIALIAS)
    bit = 8
    with_watermark = AddWatermarkFilter(original_img).apply(wtm, bit)
    grayscale = GrayscaleFilter(with_watermark).apply()
    plane = GetBitPlaneFilter(with_watermark).apply(bit)
    show_both(original_img, wtm, with_watermark, grayscale, plane)


def sobol(original_img):
    show_both(original_img, SobolFilter(original_img).apply())


def median(original_img):
    show_both(original_img, MedianFilter(original_img).apply(2))


def kernel(original_img):
    gaussian_kernel = [[0.003765, 0.015019, 0.023792, 0.015019, 0.003765],
                       [0.015019, 0.059912, 0.094907, 0.059912, 0.015019],
                       [0.023792, 0.094907, 0.150342, 0.094907, 0.023792],
                       [0.015019, 0.059912, 0.094907, 0.059912, 0.015019],
                       [0.003765, 0.015019, 0.023792, 0.015019, 0.003765]]

    resolution_kernel = [[0, -.5, 0],
                         [-.5, 3, -.5],
                         [0, -.5, 0]]
    erosion_kernel = [[0, 0, 1, 0, 0],
                      [0, 1, 1, 1, 0],
                      [1, 1, 1, 1, 1],
                      [0, 1, 1, 1, 0],
                      [0, 0, 1, 0, 0]]
    topup_kernel = [[1, 1, 0, 1, 1],
                    [1, 0, 0, 0, 1],
                    [0, 0, 0, 0, 0],
                    [1, 0, 0, 0, 1],
                    [1, 1, 0, 1, 1]]
    show_both(original_img, KernelFilter(original_img).apply(
        # gaussian_kernel
        # resolution_kernel
        erosion_kernel
        # topup_kernel
    ))


def main(mode):
    cmds_dict = {
        'invert': invert,
        'tone': add_tone,
        'extract': extract_canal,
        'blend': blend,
        'sobol': sobol,
        'kernel': kernel,
        'median': median,
        'watermark': watermark,
    }
    cmd = cmds_dict.get(mode)
    if not cmd:
        print(f'Unknown cmd "{cmd}", try to use another: {cmds_dict.keys()}')
        exit(1)
    original_img = load_image(IMAGE_PATH)
    if not original_img:
        return
    cmd(original_img)


if __name__ == '__main__':
    main(sys.argv[1])
